%global commit c9cfbb7ab873aedf8a1d5820631217bd21995db9
Name:       gnome-shell-extension-do-not-disturb
Summary:    Enable or disable do not disturb mode.
Version:    0.1.0
Release:    1%{?dist}
URL:        https://github.com/kylecorry31/gnome-shell-extension-do-not-disturb
License:    MIT
BuildArch:  noarch

Source0: https://github.com/kylecorry31/gnome-shell-extension-do-not-disturb/archive/%{commit}.tar.gz#/%{name}-%{commit}.tar.gz

Requires: gnome-shell >= 3.24
Requires: gnome-shell-extension-common

Recommends: gnome-tweak-tool

%description
Gnome-shell extension to prevent notification pop-ups

# UUID is defined in extension's metadata.json and used as directory name.
%global  UUID                  donotdisturb@kylecorry31.github.io
%global  gnome_extensions_dir  %{_datadir}/gnome-shell/extensions
%global  final_install_dir     %{buildroot}/%{gnome_extensions_dir}/%{UUID}/

%prep
#%autosetup -c %{name}-%{version}
%setup -n %{name}-%{commit}

%build
# No compiling necessary.

%install
mkdir -p %{final_install_dir}
cp --recursive --preserve=mode,timestamps  ./*  %{final_install_dir}

rm %{final_install_dir}/README.md

# RPM will take care of gschemas, we don't need to include a precompiled copy.
mkdir -p %{buildroot}/%{_datadir}/glib-2.0/schemas/
mv  %{final_install_dir}/src/schemas/org.gnome.shell.extensions.kylecorry31-do-not-disturb.gschema.xml  \
    %{buildroot}/%{_datadir}/glib-2.0/schemas/
rm --recursive %{final_install_dir}/src/schemas/

%files 
%doc README.md
%license LICENSE
%{gnome_extensions_dir}/%{UUID}/
%{_datadir}/glib-2.0/schemas/org.gnome.shell.extensions.kylecorry31-do-not-disturb.gschema.xml

%changelog
* Thu Sep 06 2018 JMiahMan <JMiahMan@unity-linux.org> - 0.1.0-1
- Initial Build
